package com.filentech.core.encrypt;

import org.junit.Test;
import org.yi.core.utils.DesUtils;

public class DesTest {
	
	private static final String KEY = "12341234";
	
	@Test
	public void testDes(){
		String text = "this is a test text";
		try {
			String t2 = DesUtils.encode(text, KEY);
			System.out.println(t2);
			System.out.println(DesUtils.decode(t2, KEY));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
