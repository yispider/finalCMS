package com.filentech.core.pattern;

import java.util.regex.Pattern;

import junit.framework.Assert;

import org.junit.Test;

public class PatternTest {

	@Test
	public void testPattern(){
		String regex = "^\\d+\\.\\d{0,2}";
		Assert.assertTrue(Pattern.matches(regex, "0.1"));
		Assert.assertTrue(Pattern.matches(regex, "123."));
		Assert.assertTrue(Pattern.matches(regex, "123.0"));
	}
	
}
