package com.filentech.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Test;
import org.yi.core.mail.MailSenderFactory;
import org.yi.core.mail.SimpleMailSender;
import org.yi.core.utils.FileUtils;
import org.yi.core.utils.PropertiesUtils;

public class MailTest {
	
	@Before
	public void init(){
		try {
			org.yi.core.common.Constants.config = PropertiesUtils.load(
					FileUtils.locateAbsolutePathFromClasspath("main.properties").getName(), "utf-8");
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
	}

	@Test
    public void testSend() {
        try {
            SimpleMailSender sender = MailSenderFactory.getSender();
            List<String> recipients = new ArrayList<String>();
        	recipients.add("dugudujiaozhu@qq.com");
        	recipients.add("1927007862@qq.com");
            for (String recipient : recipients)
                sender.send(recipient, "测试邮件", "这是一封测试邮件！");
            // sender.send(recipients, subject, content);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
