
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for f_category
-- ----------------------------
DROP TABLE IF EXISTS `f_category`;
CREATE TABLE `f_category` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '栏目名称',
  `alias` varchar(50) DEFAULT NULL COMMENT '栏目别名',
  `pid` bigint(5) DEFAULT NULL COMMENT '所属栏目号',
  `url` varchar(255) DEFAULT NULL COMMENT '栏目链接',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `create_user` varchar(40) NOT NULL COMMENT '创建人账号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for f_comment
-- ----------------------------
DROP TABLE IF EXISTS `f_comment`;
CREATE TABLE `f_comment` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) NOT NULL COMMENT '评论的文章的id',
  `quote_comment_id` bigint(20) DEFAULT NULL COMMENT '引用评论的ID',
  `comment` varchar(1000) NOT NULL COMMENT '评论内容',
  `user_name` varchar(50) NOT NULL DEFAULT '游客' COMMENT '评论人账号（默认为游客）',
  `audit_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '审核状态（0 未审核，1 审核通过， 2 审核未通过）',
  `audit_explain` varchar(255) DEFAULT NULL COMMENT '审核说明',
  `support_times` tinyint(4) unsigned DEFAULT NULL COMMENT '评论支持人数',
  `oppose_times` tinyint(4) unsigned DEFAULT NULL COMMENT '反对人数',
  `is_report` bit(1) DEFAULT NULL COMMENT '是否被举报为不良信息',
  `crate_time` datetime NOT NULL COMMENT '评论时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for f_friend_link
-- ----------------------------
DROP TABLE IF EXISTS `f_friend_link`;
CREATE TABLE `f_friend_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `logo` varchar(64) DEFAULT NULL COMMENT 'logo',
  `url` varchar(128) DEFAULT NULL COMMENT '地址',
  `keyword` varchar(32) DEFAULT NULL COMMENT '关键词',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '状态， 1=启用，0=禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for f_permission
-- ----------------------------
DROP TABLE IF EXISTS `f_permission`;
CREATE TABLE `f_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '权限名称',
  `description` varchar(255) DEFAULT NULL COMMENT '权限描述',
  `create_time` datetime NOT NULL COMMENT '权限创建时间',
  `crate_user_id` int(20) NOT NULL COMMENT '创建该权限的用户id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_roles_permission` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='用户角色表';

-- ----------------------------
-- Table structure for f_post
-- ----------------------------
DROP TABLE IF EXISTS `f_post`;
CREATE TABLE `f_post` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ftype` varchar(20) NOT NULL COMMENT '类别',
  `category` bigint(20) NOT NULL COMMENT '栏目',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `keyword` varchar(100) DEFAULT NULL COMMENT '关键词',
  `desc` varchar(255) DEFAULT NULL COMMENT '描述',
  `author` varchar(50) DEFAULT NULL COMMENT '作者',
  `content` longtext COMMENT '正文内容',
  `status` varchar(20) NOT NULL DEFAULT 'publish' COMMENT '状态',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `visit` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '阅读次数',
  `vote` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '投票数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for f_role
-- ----------------------------
DROP TABLE IF EXISTS `f_role`;
CREATE TABLE `f_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL COMMENT '角色名',
  `role_desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime NOT NULL COMMENT '角色创建时间',
  `crate_user_id` int(20) NOT NULL COMMENT '创建该角色的用户id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_roles` (`role_desc`,`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Table structure for f_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `f_role_permission`;
CREATE TABLE `f_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `permission_id` int(11) NOT NULL COMMENT '权限id',
  `create_time` datetime NOT NULL COMMENT '授予该角色权限的时间',
  `crate_user_id` int(20) NOT NULL COMMENT '授予该角色权限的管理员ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_permissin_u_key` (`role_id`,`permission_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色权限中间表';

-- ----------------------------
-- Table structure for f_sysparam
-- ----------------------------
DROP TABLE IF EXISTS `f_sysparam`;
CREATE TABLE `f_sysparam` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL COMMENT '系统参数代码',
  `name` varchar(100) NOT NULL COMMENT '系统参数名称（描述）',
  `value` varchar(500) NOT NULL COMMENT '系统参数的值',
  `effective` bit(1) NOT NULL DEFAULT b'1' COMMENT '系统参数是否有效',
  `crate_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_uk` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for f_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `f_sys_user`;
CREATE TABLE `f_sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL COMMENT '用户名',
  `passwd` varchar(32) NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for f_user
-- ----------------------------
DROP TABLE IF EXISTS `f_user`;
CREATE TABLE `f_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL COMMENT '注册邮箱',
  `passwd` varchar(32) NOT NULL COMMENT '密码',
  `salt` int(4) NOT NULL DEFAULT '1234' COMMENT '密码加密用的盐值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for f_user_role
-- ----------------------------
DROP TABLE IF EXISTS `f_user_role`;
CREATE TABLE `f_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `create_time` datetime NOT NULL COMMENT '授予用户该角色的的时间',
  `crate_user_id` int(20) NOT NULL COMMENT '授予用户该角色的管理员ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userId_roleId_unique_index` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户角色中间表';
