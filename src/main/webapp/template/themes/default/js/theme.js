// Login check
function tin_check_login(){
	if(yi.uid>0) return true;
	if($("div.overlay").length<=0) $("#content-container").append('<div class="overlay"></div>');
	$("div.overlay").show(),$("body").addClass("fadeIn");
	$('#sign').removeClass("register").addClass("sign");
	$("div.overlay, form a.close").bind("click",function(){return $("body").removeClass("fadeIn"),$('#sign').removeAttr("class"),$("div.overlay").remove();});
	return false;
};

//Tooltip
 $(function(){$(".tooltip-trigger").each(function(b){
 	if(this.title){
 		var c=this.title;
 		var a=5; 
 		$(this).mouseover(function(d){
 			this.title="";
 			$(this).append('<div class="tooltip top" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner">'+c+'</div></div>');
 			$(".tooltip").css({left:(($(this).width()-$('.tooltip').width())/2)+"px",bottom:($(this).height()+a)+"px"}).addClass('in').fadeIn(250);
 		}).mouseout(function(){this.title=c;$(".tooltip").remove();});
 	}
 });});

// Content Index
$(document).on("click","#content-index-control",
  function(){
    if($('#content-index-control').hasClass('open')){
      $('#content-index-control').html('[展开]');
      $('#content-index-control').removeClass('open');
      $('#index-ul').css('display','none');
    }else{
      $('#content-index-control').html('[收起]');
      $('#content-index-control').addClass('open');
      $('#index-ul').css('display','block');
    }
});

//Scroll to top
$(function(){
  showScroll();
  function showScroll(){
    $(window).scroll( function() { 
      var scrollValue=$(window).scrollTop();
	  if($('.floatwidget').length>0) {
        var fwbo = $('.floatwidget').offset(),
	       fwh = $('.floatwidget').height(),
           wdis = fwbo.top + fwh + 60;
        var mbh = $('#main-wrap').height(),
            mbo = $('#main-wrap').offset(),
            mb = mbo.top + mbh;
           maxh = fwh + scrollValue+110;
           if(scrollValue > wdis){
                if($('.floatwidget-container').html()==''){
                    $('.floatwidget').clone().prependTo($('.floatwidget-container'));
                }
                $('.floatwidget-container').fadeIn('slow');
                if(maxh > mb){
                  var newtop = mb-maxh+80;
                  $('.floatwidget-container').css('top',newtop);  
                }else{
                  $('.floatwidget-container').css('top',80);  
                }
            }else{
                $('.floatwidget-container').html('').fadeOut('slow');
            }
       }
      scrollValue > 200 ? $('span[id=back-to-top]').fadeIn('slow'):$('span[id=back-to-top]').fadeOut('slow');
      //fixnav
      (scrollValue > 60 && screen.width>640) ? $('#nav-scroll').addClass('tofix'):$('#nav-scroll').removeClass('tofix');

    } );  
    $('#back-to-top').click(function(){
       $("html,body").animate({scrollTop:0},1000);  
    }); 
  }
});

// 点击喜欢
$(".like-btn").click(function(){
      var _this = $(this);
      var pid = _this.attr('pid');
	  var cookie_name = 'tin_post_like_'+pid;
	  var vote_cookie = tinGetCookie(cookie_name);
      if(_this.hasClass('love-yes')||vote_cookie.length>0) return;
	  $.ajax({type: 'POST', xhrFields: {withCredentials: true}, dataType: 'json', url: yi.ajax_url + "/like", data: 'pid=' + pid, cache: false, success: function(data){var num = _this.children("span").text();_this.children("span").text(Number(num)+1);_this.addClass("love-animate").attr("title","已喜欢");tinSetCookie(cookie_name,1,3600,yi.home);setTimeout(function(){_this.removeClass('love-animate').addClass('love-yes');},500);if(data.get===1) tinAlert('参与文章互动获取'+data.credit+'积分');}});
});

//点击收藏或取消收藏
$('.collect').click(function(){
	if(!tin_check_login())return;
	var _this = $(this);
	var pid = Number(_this.attr('pid')); 
	if(_this.hasClass('collect-no')){
		$.ajax({
			type: 'POST', 
			xhrFields: {withCredentials: true}, 
			dataType: 'json', 
			url: yi.ajax_url + "/addFavorite", 
			data: 'pid=' + pid, 
			cache: false,
			success: function(r){
				if(r.result) {
					_this.children("span").text("已收藏");
					_this.addClass("collect-animate").attr("title","已收藏");
					setTimeout(function(){_this.removeClass('collect-animate').removeClass('collect-no').addClass('collect-yes').addClass('remove-collect');},500);
				} else {
					alert(r.message);
				}
			}
		});
		return false;
	}else if(_this.hasClass('remove-collect')){
		$.ajax({
			type: 'POST',
			xhrFields: {withCredentials: true},
			dataType: 'json',
			url: yi.ajax_url + "/removeFavorite",
			data: 'pid=' + pid,
			cache: false,
			success: function(r){
				if(r.result) {
					_this.children("span").text("点击收藏");
					_this.addClass("collect-animate").attr("title","点击收藏");
					setTimeout(function(){_this.removeClass('collect-animate').removeClass('remove-collect').removeClass('collect-yes').addClass('collect-no');},500);
				} else {
					alert(r.message);
				}
			}
		});
		return false;
	}else{
		return;
	}   	
});

//头像旋转
$("#main-wrap img.avatar").mouseover(function(){
	$(this).addClass("avatar-rotate");
});

$("#main-wrap img.avatar").mouseout(function(){
	$(this).removeClass("avatar-rotate");
});

//mail send download link
// Close pop-up
$(".alert_close, .alert_cancel .btn").click(function(){
    $('.tinalert').fadeOut();
});

// Alert template
function tinAlert($msg){
	var $content = '<div class="tinalert"><div class="alert_title"><h4>来自网页的提醒</h4></div><div class="alert_content"><p>'+$msg+'</p></div><div class="alert_cancel"><button class="cancel-to-back btn btn-danger">确定</button></div><span class="alert_close"><i class="fa fa-close"></i></span></div>';
	$('body').append($content);
	$(".alert_close, .alert_cancel .btn").bind('click',function(){
		$('.tinalert').fadeOut().remove();
	});
	$('.tinalert').fadeIn();
}


// Ajax post basic
var tinRefreshIcon = '<i class="fa fa-spinner fa-spin" style="margin-right:4px;line-height:20px;font-size:20px;font-size:2rem;"></i>';
function tin_do_post(formid, posturl, postdata, contentid){
	$(formid).find('[type="submit"]').addClass('disabled').prepend(tinRefreshIcon);
	$.ajax({
		type: 'POST', 
		url: posturl,
		data: postdata,
		success: function(response) {
			$(contentid).html($(response).find(contentid).html());
		},
		error: function(){
			tin_do_post(formid, posturl, postdata, contentid);
		}
	});
}

//Submit
$('#pmform').submit(function(){
	var formid = '#pmform';
	var p = $(formid);
	tin_do_post(
		formid, 
		location.href, 
		{
		'pmNonce' : p.find('[name="pmNonce"]').val(),
		'pm' : p.find('[name="pm"]').val()
		},
		'.content'
	);
	return false;
});
$('#creditform').submit(function(){
	var formid = '#creditform';
	var p = $(formid);
    var obj;
    var checked=null;       
    obj=document.getElementsByName('creditChange');   
    if(obj){
        for (var i = 0; i < obj.length; i++){
            if(obj[i].checked){
                checked = obj[i].getAttribute('value');
            }else{checked = 'add';}
        }      
    }else{checked = 'add';}
	tin_do_post(
		formid, 
		location.href, 
		{
		'creditNonce' : p.find('[name="creditNonce"]').val(),
		'creditChange' : checked,
		'creditNum' : p.find('[name="creditNum"]').val(),
		'creditDesc' : p.find('[name="creditDesc"]').val()
		},
		'.content'
	);
	return false;
});

// Add promote code
$('#promoteform').submit(function(){
	var formid = '#promoteform';
	var p = $(formid);
    var obj;
    var checked=null;       
    obj=document.getElementsByName('promote_type');   
    if(obj){
        for (var i = 0; i < obj.length; i++){
            if(obj[i].checked){
                checked = obj[i].getAttribute('value');
            }else{checked = 'once';}
        }      
    }else{checked = 'once';}
	tin_do_post(
		formid, 
		location.href, 
		{
		'promoteNonce' : p.find('[name="promoteNonce"]').val(),
		'promote_code' : p.find('[name="promote_code"]').val(),
		'promote_type' : checked,
		'discount_value' : p.find('[name="discount_value"]').val(),
		'expire_date' : p.find('[name="expire_date"]').val()
		},
		'.content'
	);
	return false;
});

// Delete promote code
$('.delete_promotecode').on('click',function(){
	var p = $(this).parent('tr').children('input[name=promote_id]');
	var promote_id = p.val();
	var dpromoteNonce = $('.promote-table input[name=dpromoteNonce]').val();
    $.ajax({
		type: 'POST', 
		url: location.href,
		data: {
			'promote_id': promote_id,
			'dpromoteNonce': dpromoteNonce
		},
		success: function(response) {
			$('.content').html($(response).find('.content').html());
		},
		error: function(){
			tinAlert('删除失败,请重新再试');
		}
	});
	return false;
});

// Subscribe
$('button#subscribe').click(function(){
	email = $('input#subscribe').val();
	if(email==''||(!email.match('^([a-zA-Z0-9_-])+((\.)?([a-zA-Z0-9_-])+)+@([a-zA-Z0-9_-])+(\.([a-zA-Z0-9_-])+)+$'))){
		$('#subscribe-msg').html('请输入正确邮箱').slideToggle('slow');
		setTimeout(function(){$('#subscribe-msg').slideToggle('slow');},2000);
	}else{
		$.ajax({type: 'POST', dataType: 'json', url: yi.ajax_url + "/subscribe", data: 'email=' + email, cache: false, success:function(){
			$('#subscribe-span').html('你已成功订阅该栏目，同时你也会收到一封提醒邮件.');
		}});
	}
});
	
// Unsubscribe
$('button#unsubscribe').click(function(){
	email = $('input#unsubscribe').val();
	if(email==''||(!email.match('^([a-zA-Z0-9_-])+((\.)?([a-zA-Z0-9_-])+)+@([a-zA-Z0-9_-])+(\.([a-zA-Z0-9_-])+)+$'))){
		$('#unsubscribe-msg').html('请输入正确邮箱').slideToggle('slow');
		setTimeout(function(){$('#unsubscribe-msg').slideToggle('slow');},2000);
	}else{
		$.ajax({type: 'POST', dataType: 'json', url: yi.ajax_url, data: 'action=unsubscribe&email=' + email, cache: false, success:function(data){
			$('#unsubscribe-span').html(data.msg);
		}});
	}
});
	
// Cookie
// function set cookie
function tinSetCookie(c_name,value,expire,path){
	var exdate=new Date();
	exdate.setTime(exdate.getTime()+expire*1000);
	document.cookie=c_name+ "=" +escape(value)+((expire==null) ? "" : ";expires="+exdate.toGMTString())+((path==null) ? "" : ";path="+path);
}
// function get cookie
function tinGetCookie(c_name){
	if (document.cookie.length>0){
		c_start=document.cookie.indexOf(c_name + "=");
		if (c_start!=-1){ 
			c_start=c_start + c_name.length+1;
			c_end=document.cookie.indexOf(";",c_start);
			if (c_end==-1) c_end=document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		}
	}
	return "";
}

// Get promoter name in url
function tinGetQueryString(name){
     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
     var r = window.location.search.substr(1).match(reg);
     if(r!=null)return  unescape(r[2]); return null;
}

// Header search slide
	$('.search-btn-click').bind('click',function(){
		if($('.header-search-slide').css('display')=='none'){
			$(this).css({'background':'#fafafa'});
			$('.header-search-slide').slideDown();
			$('.header-search-slide').children().children('input').focus();
		}
	});
	$('.header-search-slide').children().children('input').bind('blur',function(){
		$('.search-btn-click').css({'background':'transparent'});
		$('.header-search-slide').slideUp();
	});
	
// Toggle mobile menu
    var opened=false;
    $('.toggle-menu').bind('click',function(event){
		if(screen.width>640){
			$('#pri-nav ul').slideToggle();
		}
			$('#content-container').toggleClass('push');
			$('#navmenu-mobile-wraper').toggleClass('push');
			if(opened){
				opened=false;
				setTimeout(function(){
					$('#navmenu-mobile').removeClass('push');
				},500);
			}else{
				$('#navmenu-mobile').addClass('push');
				opened=true;
			}
    });
    $('#main-wrap').bind('click',function(){
        if(screen.width<=640 && opened==true){
            $('#content-container').toggleClass('push');
            $('#navmenu-mobile-wraper').toggleClass('push');
            setTimeout(function(){
                $('#navmenu-mobile').removeClass('push');
            },500);
            opened=false;
        }
    });

// Toggle sortpage menu
$('#page-sort-menu-btn a').click(function(){
	$('.pagesidebar ul').slideToggle();
});
	
// Slide nav
$(".menu-item-has-children").bind('mouseover',function(){
	if(screen.width>640&&!$(this).children('ul').is(":animated")) $(this).children('ul').slideDown(); 
}).bind('mouseleave',function(){
	if(screen.width>640) $(this).children('ul').slideUp(); 
});

$('.login-yet-click').bind('mouseover',function(){
	if(!$(this).children('.user-tabs').is(":animated"))$(this).children('.user-tabs').slideDown();
}).bind('mouseleave',function(){
	$(this).children('.user-tabs').slideUp();
});

// Slide focus-us
$('#focus-us').bind('mouseover',function(){
	if(!$(this).children('#focus-slide').is(":animated"))$(this).children('#focus-slide').slideDown();
}).bind('mouseleave',function(){
	$(this).children('#focus-slide').slideUp();
});

//Toggle smiles
$('.comt-smilie').bind('click',function(){
	$('#comt-format').hide();
    $('#comt-smilie').toggle();
});
$('.comt-format').bind('click',function(){
    $('#comt-smilie').hide();
    $('#comt-format').toggle();
});

//general popup
$("[data-pop]").on("click", function() {
    var b = $(this).attr("data-pop");
    $("div.overlay").length<=0?$(".header-wrap").before('<div class="overlay"></div>'):$("div.overlay").show();
    $(".popupbox").hide(), $("#" + b).fadeIn();
});

$("[data-top]").on("click", function() {
    var b = $(this).attr("data-top");
    "true" == b && $("body,html").animate({
        scrollTop: 0
    }, 0);
});

$('body').on("click","div.overlay,a.popup-close",function(){
    $("div.popupbox, div.overlay").fadeOut();
});

// amount plus or minus
function calculate() {
    $("#total-price").find("strong").text("￥" + Number($("#order_quantity").val() * $("#order_price").val()).toFixed(2));
}
$("div.amount-number a").on("click", function(b) {
    b.preventDefault(), fieldName = $(this).attr("field"), fieldstyle = $(this).attr("id");
    var c = parseInt($("input[name=" + fieldName + "]").val());
	var d = parseInt($('li.summary-amount span.dt-num').text());
    "plus" == fieldstyle ? Number(c) >= d ? $("input[name=" + fieldName + "]").val(d) : isNaN(c) ? $("input[name=" + fieldName + "]").val(0) : $("input[name=" + fieldName + "]").val(c + 1) : "minus" == fieldstyle && (!isNaN(c) && c > 1 ? $("input[name=" + fieldName + "]").val(c - 1) : $("input[name=" + fieldName + "]").val(1));
}), 
$("input[name=amountquantity]").keyup(function() {
    var c = $(this).val();
	var d = parseInt($('li.summary-amount span.dt-num').text());
    if(!/^(\+|-)?\d+$/.test(c) || 0 >= c) $(this).val(1);
	if(d<c) $(this).val(d);
}), 
$("a.buy-btn").on("click", function(b) {
    b.preventDefault(), $("input[name=order_quantity]").val($("input[name=amountquantity]").val()), calculate();
}),
$("#order_quantity").keyup(function() {
    var c = $(this).val();
    "" == c ? $(this).val(1) : ($("#pay-submit").removeAttr("disabled"), (!/^(\+|-)?\d+$/.test(c) || 0 >= c) && $(this).val(1)),calculate();
});

// Document ready
// -------------------- //
$(document).ready(function(){
  // slider video play
  	$('.slider-video').find('span').bind('click',function(){
		var ifs=$(this).parent('a'),
  		url = ifs.attr('data');
		if(ifs.find('embed').length==0){
    	if($('#flexslider').children('ul').children('li').length>1)jQuery( '#flexslider' ).flexslider( 'pause' );
			ifs.append('<embed src="'+url+'" width=100% height=100% align="middle" allowScriptAccess="never" allowNetworking="internal" allowfullscreen="true" autostart="0" type="application/x-shockwave-flash" flashvars="isShowRelatedVideo=false&showAd=0&isAutoPlay=true&isDebug=false" />');
    		ifs.parent('.slider-video').addClass('playing');
		}else{
    		$(this).parent('a').find('embed').remove();
    		$(this).parent('a').parent('.slider-video').removeClass('playing');
    		jQuery( '#flexslider' ).flexslider( 'play' );
  		}
  	});
	//Toggle Content
	$('.toggle-click-btn').click(function(){
		$(this).next('.toggle-content').slideToggle('slow');
        if($(this).hasClass('yes')){
            $(this).removeClass('yes');
            $(this).addClass('no');
        }else{
            $(this).removeClass('no');
            $(this).addClass('yes');
        }
	});

    //Archves page
    (function(){
         $('#archives span.al_mon').each(function(){
             var num=$(this).next().children('li').size();
             var text=$(this).text();
             $(this).html(text+'<em> ( '+num+' 篇文章 )</em>');
         });
         var $al_post_list=$('#archives ul.al_post_list'),
             $al_post_list_f=$('#archives ul.al_post_list:first');
         $al_post_list.hide(1,function(){
             $al_post_list_f.show();
         });
         $('#archives span.al_mon').click(function(){
             $(this).next().slideToggle('slow');
             return false;
         });
         $('#al_expand_collapse').toggle(function(){
             $al_post_list.show();
         },function(){
             $al_post_list.hide();
         });
     })();

     //Title loading 
     $('h3 a').click(function(){
        myloadoriginal = this.text;
        $(this).text('请稍等，正在努力加载中...');
        var myload = this;
        setTimeout(function() { $(myload).text(myloadoriginal); }, 2000);
    });
	
	//Infobg close
	$('.infobg-close').click(function(){
		$(this).parent('.contextual').fadeOut('slow');
	});

    //Marquee site news
    function startmarquee(lh,speed,delay,index){ 
        var t=0; 
        var p=false; 
        var o=document.getElementById("news-scroll-zone"+index); 
        o.innerHTML+=o.innerHTML; 
        o.onmouseover=function(){p=true;} ;
        o.onmouseout=function(){p=false;} ;
        o.scrollTop = 0; 
        function start(){ 
            t=setInterval(scrolling,speed); 
            if(!p){ o.scrollTop += 1;} 
        } 
        function scrolling(){ 
            if(o.scrollTop%lh!=0){ 
            o.scrollTop += 1; 
            if(o.scrollTop>=o.scrollHeight/2)
                o.scrollTop = 0; 
            }else{ 
                clearInterval(t); 
                setTimeout(start,delay); 
            } 
        } 
        setTimeout(start,delay); 
    }
    if($('#news-scroll-zone').length>0){
        startmarquee(20,30,5000,'');
    }  
	
	//Lazyload
	if($("img.box-hide"))$("img.box-hide").lazyload({
          placeholder : yi.tin_url +"/images/image-pending.gif",
          threshold : 50,
          failure_limit : 100,
          skip_invisible : false
    });
	
	// action tin affiliate url and trackback url
	$('.tin_aff_url,.trackback-url,input[name=rss]').click(function(){
		$(this).select();
	});
	$('.quick-copy-btn').click(function(){
		$(this).parent().children('input').select();
		alert('请用CTRL+C复制');
	});
	
	// action set affiliate cookie ( credit )
	if(tinGetQueryString('aff')) tinSetCookie('tin_aff',tinGetQueryString('aff'),86400,yi.home);
	
	// go to comment
	$('.commentbtn').click(function(){$('html,body').animate({scrollTop:$('#comments').offset().top}, 800);});
	
	// loading complete
	$('.site_loading').fadeOut();

    // comments tabs
    $('.commenttitle span').click(function(e){
        $('.commenttitle span').removeClass('active');
        $(this).addClass('active');
        $('.commentlist').hide();
        $($(this).parent('a').attr('href')).fadeIn();
        e.preventDefault();
    });
	
	// stickys & latest tabs
    $('.stickys span.heading-text-cms').click(function(e){
        $('.stickys span.heading-text-cms').removeClass('active');
        $(this).addClass('active');
        $('.stickys-latest-list').hide();
        $($(this).attr('id')).fadeIn();
        e.preventDefault();
    });
	
    // Mobile nav append
    $('#menu-mobile li.menu-item-has-children').prepend('<span class="child-menu-block"></span>');
    // Mobile menu click toggle
    $('.child-menu-block').on('click',function(){
        $(this).parent().children('.sub-menu').slideToggle();
        if($(this).parent().hasClass('expand')){
            $(this).parent().removeClass('expand');
        }else{
            $(this).parent().addClass('expand');
        }
    });
	
	// href add _blank
	var titles = $('h1 a,h2 a,h3 a');
	titles.each(function(){
		$(this).attr('target','_blank');
	});
	
	// Tabs widget
	$(function() {
		var $tabsNav       = $('.tin-tabs-nav'),
			$tabsNavLis    = $tabsNav.children('li');

		if($tabsNav.length>0)$tabsNav.each(function() {
			var $this = $(this);
			$this.next().children('.tin-tab').stop(true,true).hide()
			.siblings( $this.find('a').attr('href') ).show();
			$this.children('li').first().addClass('active').stop(true,true).show();
		});

		$tabsNavLis.on('mouseover', function(e) {
			var $this = $(this);
			if($this.hasClass('active')) return;
			$this.siblings().removeClass('active').end()
			.addClass('active');
			
			$this.parent().next().children('.tin-tab').stop(true,true).hide()
			.siblings( $this.find('a').attr('href') ).fadeIn();
			e.preventDefault();
		});//.children( window.location.hash ? 'a[href=' + window.location.hash + ']' : 'a:first' ).trigger('click');
		$tabsNavLis.on('click', function(e) {
			e.preventDefault();
		});
	});
	
	// Store switch
	$(function() {
        var $wrapNav       = $('#wrapnav ul.nav'),
            $wrapNavLis    = $wrapNav.children('li');
        if($wrapNav.length>0)$wrapNav.each(function() {
            var $this = $(this);
            $this.children('li').first().addClass('active');
            $($this.find('a').attr('href')).show();
        });

        $wrapNavLis.on('click', function(e) {
            var $this = $(this);
            if($this.hasClass('active')) return;
            $this.siblings().removeClass('active').end().addClass('active');
            $this.parent().parent().next().children('.wrapbox').stop(true,true).hide().siblings( $this.find('a').attr('href') ).fadeIn();
            e.preventDefault();
        });//.children( window.location.hash ? 'a[href=' + window.location.hash + ']' : 'a:first' ).trigger('click');

	});
	
	// ajax more
	var $maxclick = 0,
		$doing = false;
	function ajax_load_data(e,p){
		if(p&&p==1)e.preventDefault();
		var $this = $('.aload a'),
			$loader = $('.aload_loading');
		$loader.show();
		$this.text('加载中...');
		$.ajax({
			type: "GET",
			async: false,
			url: $this.attr('href') + '#fluid_blocks',
			dataType: "html",
			success: function(out){
				result = $(out).find('#fluid_blocks span.masonry-box');
				nextlink = $(out).find('.aload a').attr('href');
				$maxclick++;
				$('#fluid_blocks').append(result);
				setTimeout(function(){$loader.fadeOut();result.imagesLoaded(function(){
					result.fadeIn(1000);
					$('#fluid_blocks').masonry('appended',result);
				});if(nextlink != undefined && $maxclick <5){
					$this.attr('href',nextlink);
				}else if(nextlink != undefined){
					$this.attr('href',nextlink);
					$loader.remove();
					$this.text('加载更多');
					$('.aload').fadeIn();
				}else{
					$loader.remove();
					$this.remove();
					$('.aload').removeClass('aload').addClass('bload').fadeIn().html('<a style="cursor:default;">没有更多了...</a>');
				}$doing = false;},200);				
			}
		});
	};
	
	$(window).scroll(function(){
		if($('#fluid_blocks').length>0){
			$top = $('#fluid_blocks').offset().top;
			$h = $('#fluid_blocks').height();
			if(($(document).height()-$(this).scrollTop()-$(this).height()<100||$top + $h -$(this).scrollTop()<450) && $maxclick <5 && $doing == false){
				$doing = true;
				ajax_load_data();
			}
		}
	});
	
	$('.aload a').on('click',function(e){
		ajax_load_data(e,1);
	});

	// radio page box switch
	$('.box-control li').on('click',function(){
		var box = $('#'+$(this).attr('data'));
		$(this).parent().children('li').removeClass('active');
		$(this).addClass('active');
		$('.box').hide();
		box.show();
	});
	/**login and regist**/
	var b = $("#sign");
    $("#register-active").on("click", function() {
        b.removeClass("sign").addClass("register");
    }), $("#login-active").on("click", function() {
        b.removeClass("register").addClass("sign");
    }), $(".user-login,.user-reg").on("click", function(c) {
        $("div.overlay").length <= 0 ? $(".header-wrap").before('<div class="overlay"></div>') : $("div.overlay").show(), $("body").addClass("fadeIn"), 1 == $(this).attr("data-sign") ? b.removeClass("sign").addClass("register") : b.removeClass("register").addClass("sign"), $("div.overlay, form a.close").on("click", function() {
            return $("body").removeClass("fadeIn"), b.removeAttr("class"), $("div.overlay").remove(), !1;
        }), c.preventDefault();
    });
    $('#login').validate({
		submitHandler:function(form){
		      $(form).ajaxSubmit({
		    	  type:'POST',
		    	  dataType:"json",
		    	  url:ajax_sign_object.ajaxurl+"/login?time="+ (new Date()).getTime(),
		    	  success: function(data){
		    		  if(data.result){
		    			  location.reload();
		    		  }else{
		    			  $('.status').show().text(data.message);
		    			  return false;
		    		  }
		    	  }
		      });
		},
		rules:{
			email:{
				required: true,
				email:true,
			}
		},
		messages:{
			email: {
				required: "请输入邮箱",
				email:"请输入合法的email地址！"
		    }
		},
		showErrors : function(errorMap, errorList) {
			var msg = '';  
			$.each(errorList, function(i, v) {
				msg += (v.message + '\r\n'); 
				return false; 
			});  
			if (msg != '')  
				$('.status').show().text(msg);
		},  
		/* 失去焦点时不验证 */  
		onfocusout : false  
	});
	
	/**
	 * 退出
	 */
	$('.user-logout').click(function(){
		$.ajax({
			type : "post",
			dataType : "json",
			url : ajax_sign_object.ajaxurl+"/logout",
			success:function(data){
				if(data){
					location.reload();
				}
			}
		});
	});
	/**
	 * 注册
	 */
	$('#register').validate({
		submitHandler:function(form){
		      $(form).ajaxSubmit({
		      	type:'POST',
		        dataType:"json",
		        url:ajax_sign_object.ajaxurl+"/regist?time="+ (new Date()).getTime(),
		        success: function(data){
		        	if(data.result){
		        		location.reload();
		        	} else {
		        		$('.status').show().text(data.message);
		        		return false;
		        	}
		        }
		      });
		},
		rules:{
			username:{
				required: true,
				rangelength: [5,16],
				remote:{
					type:"POST",
					dataType: "json",
					url: ajax_sign_object.ajaxurl+"/userExist?time="+ (new Date()).getTime(),
					data:{key:function(){return $("#username").val();}},
					dataFilter: function(data) {
						if (data=="true")
	                        return true;
	                    else
	                        return false;
                	}
				}
			},
			user_pass:{
				required: true,
				rangelength: [6,16]
			},
			re_pass:{
				required: true,
				rangelength: [6,16],
				equalTo: "#user_pass"
			},
			user_email:{
				required: true,
				email:true,
				remote:{
					type:"post",
					url: ajax_sign_object.ajaxurl+"/emailExist?time="+ (new Date()).getTime(),
					data:{key:function(){return $("#user_email").val();}},
					dataType:"json",
					dataFilter: function(data) {
						if (data=="true")
                    		return true;
                    	else
                        	return false;
                	}
				}
			}
		},
		messages:{
			username:{
				required: "请输入用户名！",
				rangelength: "用户名必须在5-16个字符之间！",
				remote: "用户名存在，不可用！"
			},
			user_pass: {
				required: "请输入密码！",  
				rangelength: "密码必须在6-16个字符之间！"
			},  
			re_pass: {  
				required: "请输入确认密码！",  
				rangelength: "确认密码必须在6-16个字符之间！", 
				equalTo: "两次输入密码不一致！"  
			},
			user_email:{
				required: "请输入邮箱！",
				email:"请输入合法的email地址！",
				remote: "邮箱存在，不可用！"
			}
		},
		showErrors : function(errorMap, errorList) {  
			var msg = '';  
			$.each(errorList, function(i, v) {  
				msg += (v.message + '\r\n'); 
				return false; 
			});  
			if (msg != '')  
				$('.status').show().text(msg);
		},  
		/* 失去焦点时不验证 */  
		onfocusout : false
	});
});
function footer() {
	document.writeln("<div id=\"footer-wrap\">");
	document.writeln("	<div id=\"footer\" class=\"layout-wrap\">");
	document.writeln("		<!-- Footer Widgets -->");
	document.writeln("		<div id=\"footer-widgets\">");
	document.writeln("			<div class=\"footer-widgets-1 footer-widgets-one-third \">");
	document.writeln("				<div class=\"widget widget_tinaboutsite\">");
	document.writeln("					<h3><span class=widget-title>关于本站</span></h3>");
	document.writeln("					<div class=\"aboutsite\">易大师 www.yispider.com是易大师采集器官方博客， 主要用于分享作者对于软件的一些介绍， 技术上的一些记录， 以及生活上的一些感悟。");
	document.writeln("					</div>	");
	document.writeln("				</div>");
	document.writeln("			</div>");
	document.writeln("			<div class=\"footer-widgets-2 footer-widgets-one-third \">");
	document.writeln("                <div id=\"tinjoinus-2\" class=\"widget widget_tinjoinus\">");
	document.writeln("                    <h3><span class=\"widget-title\">联系站长</span></h3>");
	document.writeln("                    <div class=\"joinus\">如果您对本站有任何疑问， 请先登录留言或者通过QQ联系站长。");
	document.writeln("                    </div>");
	document.writeln("                </div>");
	document.writeln("            </div>");
	document.writeln("			<div class=\"footer-widgets-3 footer-widgets-one-third \">");
	document.writeln("                <div id=\"tinjoinus-2\" class=\"widget widget_tinjoinus\">");
	document.writeln("                    <h3><span>站务合作</span></h3>");
	document.writeln("                    <div class=\"sitecooperate\">如果您有站务合作方面的需求，请通过以下方式联系我。<br>QQ: 1927007862<br>Email: 1927007862#qq.com(#换成@)");
	document.writeln("                    </div>");
	document.writeln("                </div>");
	document.writeln("            </div>");
	document.writeln("		</div>");
	document.writeln("		<div class=\"clr\"></div>");
	document.writeln("		<!-- /.Footer Widgets -->");
	document.writeln("	</div>");
	document.writeln("</div>");
	document.writeln("<!-- /.Footer Wrap -->");
	document.writeln("<footer id=\"footer-nav-wrap\">");
	document.writeln("	<div id=\"footer-nav\" class=\"layout-wrap\">");
	document.writeln("		<div id=\"footer-nav-left\">");
	document.writeln("			<div id=\"footermenu\" class=\"menu-footer-container\">");
	document.writeln("				<ul id=\"footer-nav-links\" class=\"footermenu\">");
	document.writeln("					<li><a href=\""+yi.home+"/contact\">站务合作</a></li>");
	document.writeln("					<li><a href=\""+yi.home+"/mianze\">免责声明</a></li>");
	document.writeln("					<li><a href=\""+yi.home+"/about\">关于</a></li>");
	document.writeln("					<li><a target=\"_blank\" href=\""+yi.home+"/admin/account\">管理</a></li>");
	var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
	document.write(unescape("%3Cspan id='cnzz_stat_icon_1257041642'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s11.cnzz.com/stat.php%3Fid%3D1257041642' type='text/javascript'%3E%3C/script%3E"));
	document.writeln("				</ul>");
	document.writeln("			</div>");
	document.writeln("			<div id=\"footer-copyright\">YISPIDER 2016-2018");
	document.writeln("				<a href=\"#\" target=\"_blank\">渝ICP备14010376号</a>");
	document.writeln("			</div>");
	document.writeln("		</div>");
	document.writeln("	</div>	");
	document.writeln("</footer>");
}

function sign() {
	document.writeln("<div id=\"sign\" class=\"sign\">");
	document.writeln("    <div class=\"part loginPart\">");
	document.writeln("	    <form id=\"login\" action=\"#\" method=\"post\" novalidate=\"novalidate\">");
	document.writeln("	        <div id=\"register-active\" class=\"switch\"><i class=\"fa fa-toggle-on\"></i>切换注册</div>");
	document.writeln("	        <h3>登录<p class=\"status\" style=\"color:red;\"></p></h3>");
	document.writeln("	        <p>");
	document.writeln("	            <label class=\"icon\" for=\"email\"><i class=\"fa fa-user\"></i></label>");
	document.writeln("	            <input class=\"input-control\" id=\"email\" type=\"email\" placeholder=\"请输入邮箱\" name=\"email\" required=\"\" aria-required=\"true\"/>");
	document.writeln("	        </p>");
	document.writeln("	        <p>");
	document.writeln("	            <label class=\"icon\" for=\"password\"><i class=\"fa fa-lock\"></i></label>");
	document.writeln("	            <input class=\"input-control\" id=\"password\" type=\"password\" placeholder=\"请输入密码\" name=\"password\" required=\"\" aria-required=\"true\"/>");
	document.writeln("	        </p>");
	document.writeln("	        <p class=\"safe\">");
	document.writeln("	            <label class=\"remembermetext\" for=\"rememberme\">");
	document.writeln("	            <input name=\"rememberme\" type=\"checkbox\" checked=\"checked\" id=\"rememberme\" class=\"rememberme\" value=\"forever\"/>记住我的登录");
	document.writeln("	            </label>");
	document.writeln("	            <a class=\"lost\" href=\""+yi.home+"/user/goFindPwd\">忘记密码 ?</a>");
	document.writeln("	        </p>");
	document.writeln("	        <p>");
	document.writeln("	            <input class=\"submit\" type=\"submit\" value=\"登录\" name=\"submit\"/>");
	document.writeln("	        </p>");
	document.writeln("	        <a class=\"close\"><i class=\"fa fa-times\"></i></a>");
	document.writeln("		</form>");
	document.writeln("        <div class=\"other-sign\">");
	document.writeln("			<p>您也可以使用第三方帐号快捷登录</p>");
	document.writeln("			<div class=\"2-col\">");
	document.writeln("				<a class=\"qqlogin\" href=\""+yi.home+"/user/gosso?connect=qq&redirect="+yi.home+"\"><i class=\"fa fa-qq\"></i><span>Q Q 登 录</span></a>");
	document.writeln("			</div>");
	document.writeln("			<div class=\"2-col\">");
	document.writeln("				<a class=\"weibologin\" href=\""+yi.home+"/user/gosso?connect=weibo&redirect="+yi.home+"\"><i class=\"fa fa-weibo\"></i><span>微 博 登 录</span></a>");
	document.writeln("			</div>");
	document.writeln("		</div>");
	document.writeln("    </div>");
	document.writeln("    <div class=\"part registerPart\">");
	document.writeln("    <form id=\"register\" action=\"#\" method=\"post\" novalidate=\"novalidate\">");
	document.writeln("        <div id=\"login-active\" class=\"switch\"><i class=\"fa fa-toggle-off\"></i>切换登录</div>");
	document.writeln("        <h3>注册<p class=\"status\" style=\"color:red;\"></p></h3>    ");
	document.writeln("        <p>");
	document.writeln("            <label class=\"icon\" for=\"username\"><i class=\"fa fa-user\"></i></label>");
	document.writeln("            <input class=\"input-control\" id=\"username\" type=\"text\" name=\"username\" placeholder=\"输入英文用户名\" required=\"\" aria-required=\"true\"/>");
	document.writeln("        </p>");
	document.writeln("        <p>");
	document.writeln("            <label class=\"icon\" for=\"user_email\"><i class=\"fa fa-envelope\"></i></label>");
	document.writeln("            <input class=\"input-control\" id=\"user_email\" type=\"email\" name=\"user_email\" placeholder=\"输入常用邮箱\" required=\"\" aria-required=\"true\"/>");
	document.writeln("        </p>");
	document.writeln("        <p>");
	document.writeln("            <label class=\"icon\" for=\"user_pass\"><i class=\"fa fa-lock\"></i></label>");
	document.writeln("            <input class=\"input-control\" id=\"user_pass\" type=\"password\" name=\"user_pass\" placeholder=\"密码最小长度为6\" required=\"\" aria-required=\"true\"/>");
	document.writeln("        </p>");
	document.writeln("        <p>");
	document.writeln("            <label class=\"icon\" for=\"re_pass\"><i class=\"fa fa-retweet\"></i></label>");
	document.writeln("            <input class=\"input-control\" type=\"password\" id=\"re_pass\" name=\"re_pass\" placeholder=\"再次输入密码\" required=\"\" aria-required=\"true\"/>");
	document.writeln("        </p>");
	document.writeln("        <p id=\"captcha_inline\">");
	document.writeln("            <input class=\"submit inline\" type=\"submit\" value=\"注册\" name=\"submit\"/>");
	document.writeln("        </p>");
	document.writeln("        <a class=\"close\"><i class=\"fa fa-times\"></i></a>  ");
	document.writeln("    </form>");
	document.writeln("    </div>");
	document.writeln("    <div class=\"clear\"></div>");
	document.writeln("</div>");
}
