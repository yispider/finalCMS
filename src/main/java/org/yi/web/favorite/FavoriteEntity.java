package org.yi.web.favorite;

import java.util.List;

import org.yi.core.annotation.TableBind;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;

@TableBind(name = "f_favorite")
public class FavoriteEntity extends Model<FavoriteEntity> {

	private static final long serialVersionUID = -4545730077011213863L;

	public static FavoriteEntity dao = new FavoriteEntity();

	public void deleteByUidAid(Object uid, Object aid) {
		String sql = "delete from f_favorite where user_id = ? and article_id = ?";
		Db.update(sql, uid, aid);
	}
	
	public FavoriteEntity findByUidAid(Object uid, Object aid) {
		String sql = "select * from f_favorite where user_id = ? and article_id = ?";
		return dao.findFirst(sql, uid, aid);
	}

	public List<FavoriteEntity> getByUid(Object uid) {
		String sql = "select * from f_favorite where user_id = ?";
		return dao.find(sql, uid);
	}
	
}
