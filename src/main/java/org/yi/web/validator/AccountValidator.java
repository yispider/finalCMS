package org.yi.web.validator;

import java.util.regex.Pattern;

import org.yi.web.account.entity.AccountEntity;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.validate.Validator;

public class AccountValidator extends Validator {

	@Override
	protected void validate(Controller c) {
		validateString("agentEntity.loginname", 1, 30, "error_loginname", "length of loginname must great than 6 and less than 30");
		validateString("agentEntity.firstname", 1, 30, "error_firstname", "length of firstname must great than 1 and less than 30");
		validateString("agentEntity.passwd", 6, 32, "error_passwd", "length of password must great than 6 and less than 32");
		if(StrKit.notBlank(c.getPara("agentEntity.email"))) {
			validateEmail("agentEntity.email", "error_email", "email format error");
		}
		if(!Pattern.matches("^\\d+\\.?\\d{0,2}", c.getPara("agentEntity.credit"))) {
			c.setAttr("error_credit", "credit must be a number and almost 2 decimal place");
		}
	}

	@Override
	protected void handleError(Controller c) {
		//use controller.keepmodel to return the model to the frontend
		//use controller.keeppara to return the param to the frontend
		c.keepModel(AccountEntity.class);
		c.render("add.html");
	}
	
}
