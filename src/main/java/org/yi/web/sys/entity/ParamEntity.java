package org.yi.web.sys.entity;

import java.util.List;

import org.yi.core.annotation.TableBind;

import com.jfinal.plugin.activerecord.Model;

@TableBind(name="f_param")
public class ParamEntity extends Model<ParamEntity>{

	private static final long serialVersionUID = -7043855063466129705L;

	public static ParamEntity dao = new ParamEntity();

	public List<ParamEntity> getAll() {
		String sql = "select * from f_param order by id asc";
		return dao.find(sql);
	}

	public ParamEntity getByCode(String code) {
		String sql = "select * from f_param where code = ?";
		return dao.findFirst(sql, code);
	}
	
}
