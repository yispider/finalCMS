package org.yi.web.block.entity;

import java.util.List;

import org.yi.core.annotation.TableBind;
import org.yi.core.enums.BlockTargetEnum;

import com.jfinal.plugin.activerecord.Model;

@TableBind(name="f_block")
public class BlockEntity extends Model<BlockEntity>{

	private static final long serialVersionUID = 6714255487790566747L;
	
	public static BlockEntity dao = new BlockEntity();

	public List<BlockEntity> getAll() {
		String sql = "select * from f_block order by id desc";
		return find(sql);
	}

	public BlockEntity getByName(String name) {
		String sql = "select * from f_block where name = ?";
		return dao.findFirst(sql, name);
	}
	
	public List<BlockEntity> getByTarget(BlockTargetEnum target) {
		String sql = "(select * from f_block where target = " 
					+ BlockTargetEnum.ALL.getCode() + " order by id desc) union (select * from f_block where target = " 
					+ target.getCode() + " order by id desc)";
		return find(sql.toString());
	}

}
