package org.yi.web.account.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.yi.core.annotation.TableBind;
import org.yi.core.enums.AccountStateEnums;
import org.yi.core.exceptions.NotUniqueResultException;
import org.yi.core.model.Pagination;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@TableBind(name = "f_account")
public class AccountEntity extends Model<AccountEntity>{
	
	private static final long serialVersionUID = -4368208647272740379L;
	
	private static final String TABLE_NAME = "f_account";
	
	public static final AccountEntity dao = new AccountEntity();
	
	/**
	 * get account by loginname and password
	 * @param loginname
	 * @param pw
	 * @return
	 */
	public List<AccountEntity> getAll(){
		String sql = "select * from " + TABLE_NAME + " order by id desc";
		List<AccountEntity> list = dao.find(sql);
		return list;
	}
	
	/**
	 * 根据邮箱和密码获取用户
	 * @param username
	 * @param pw
	 * @return
	 * @throws NotUniqueResultException 
	 */
	public AccountEntity getByNamePwd(String loginname, String pw) {
		String sql = "select * from " + TABLE_NAME + " where login_name = ? and passwd = ?";
		return findFirst(sql,loginname);
	}
	
	public AccountEntity findBySSO(String ssoId, String ssoType) {
		String sql = "select * from " + TABLE_NAME + " where sso_id = ? and sso_type = ?";
		return findFirst(sql, ssoId, ssoType);
	}
	
	/**
	 * 判断用户是否存在， 存在返回true， 否则返回false
	 * @param loginname
	 * @return
	 */
	public boolean userExist(String loginname) {
		String sql = "select count(*) from " + TABLE_NAME + " where login_name = ?";
		return Db.queryLong(sql, loginname) == 0;
	}
	
	public boolean emailExist(String email) {
		String sql = "select count(*) from " + TABLE_NAME + " where email = ?";
		return Db.queryLong(sql, email) == 0;
	}
	
	/**
	 * 根据用户名获取用户
	 * @param username
	 * @return
	 */
	public AccountEntity getByName(String loginname){
		String sql = "select * from " + TABLE_NAME + " where login_name = ?";
		return findFirst(sql,loginname);
	}
	
	public AccountEntity getByEmail(String email) {
		String sql = "select * from " + TABLE_NAME + " where email = ?";
		return findFirst(sql, email);
	}


	public Map<String, String> getIdTitleMap() {
		Map<String, String> map = new HashMap<String, String>();
		String sql = "select * from f_account";
		List<AccountEntity> list = dao.find(sql);
		for(AccountEntity a: list) {
			map.put("a"+a.getLong("id"), a.getStr("login_name"));
		}
		return map;
	}

	public Page<AccountEntity> getPager(Pagination pager, Map<String, String> p) {
		String select = "select * ";
		StringBuffer where = new StringBuffer("from f_account where 1=1");
		List<Object> params = new ArrayList<Object>();
		if(StringUtils.isNotBlank(p.get("login_name"))) {
			where.append("and login_name = ?");
			params.add(p.get("login_name"));
		}
		if(StringUtils.isNotBlank(p.get("email"))){
			where.append("and email = ?");
			params.add(p.get("email"));
		}
		if(StringUtils.isNotBlank(p.get("role"))){
			where.append("and role = ?");
			params.add(p.get("role"));
		}
		if(StringUtils.isNotBlank(p.get("state"))){
			where.append("and state = ?");
			params.add(p.get("state"));
		}
		if(StringUtils.isNotBlank(p.get("createTimeMin"))){
			where.append("and create_time >= ?");
			params.add(p.get("createTimeMin"));
		}
		if(StringUtils.isNotBlank(p.get("createTimeMax"))){
			where.append("and create_time < ?");
			params.add(p.get("createTimeMax"));
		}
		where.append(" order by create_time desc");
		return dao.paginate(pager.getPn(), pager.getSize(), select, where.toString(), params.toArray());
	}

	public int lock(String id) {
		String sql = "update f_account set state = " + AccountStateEnums.LOCK.getCode() + " where id = ?";
		return Db.update(sql, id);
	}
	
	public int unlock(String id) {
		String sql = "update f_account set state = " + AccountStateEnums.NORMAL.getCode() + " where id = ?";
		return Db.update(sql, id);
	}

	public void delete(String i) {
		dao.deleteById(new Object[]{i});
	}
	public void delete(String[] i) {
		dao.deleteById((Object[])i);
	}

}
