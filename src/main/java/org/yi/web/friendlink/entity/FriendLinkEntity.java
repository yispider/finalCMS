package org.yi.web.friendlink.entity;

import java.util.List;

import org.yi.core.annotation.TableBind;
import org.yi.core.common.Constants;
import org.yi.core.enums.AccountStateEnums;
import org.yi.core.enums.CommonStateEnum;
import org.yi.core.model.Pagination;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

@TableBind(name="f_friendlink")
public class FriendLinkEntity extends Model<FriendLinkEntity>{
	
	private static final long serialVersionUID = 1L;
	
	public static final FriendLinkEntity dao = new FriendLinkEntity();
	
	/**
	 * query by pagination
	 * @param pager
	 * @return
	 */
	public Page<FriendLinkEntity> getPager(Pagination pager) {
		return dao.paginate(pager.getPn(), pager.getSize(), "select *", "from f_friendlink where state = '"+CommonStateEnum.ENABLE.getCode()+"' order by id");
	}

	/**
	 * delete params
	 * @param ids
	 */
	public void delete(Long... ids) {
		for(Long id : ids) {
			deleteById(id);
		}
	}

	/**
	 * lock params
	 * @param ids
	 */
	public void lock(Long... ids) {
		String sql = "update f_friendlink set status = ? where id = ?";
		Object[][] params = new Object[ids.length][];
		for(int i=0;i<ids.length;i++) {
			params[i][0] = AccountStateEnums.LOCK.getCode();
			params[i][1] = ids[i];
		}
		Db.batch(sql, params, Constants.DEFAULT_BATCH_SIZE);
	}

	/**
	 * unlock params
	 * @param ids
	 */
	public void unlock(Long... ids) {
		String sql = "update f_friendlink set status = ? where id = ?";
		Object[][] params = new Object[ids.length][];
		for(int i=0;i<ids.length;i++) {
			params[i][0] = AccountStateEnums.NORMAL.getCode();
			params[i][1] = ids[i];
		}
		Db.batch(sql, params, Constants.DEFAULT_BATCH_SIZE);
	}

	public FriendLinkEntity getByKeyword(String k) {
		String sql = "select * from f_friendlink where keyword = ?";
		return dao.findFirst(sql, k);
	}
	
	public FriendLinkEntity getByUrl(String u) {
		String sql = "select * from f_friendlink where url = ?";
		return dao.findFirst(sql, u);
	}

	public List<FriendLinkEntity> getAll() {
		return dao.find("select * from f_friendlink where state = '"+CommonStateEnum.ENABLE.getCode()+"' order by id");
	}
}
