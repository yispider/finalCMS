package org.yi.web.ueditor;

import org.yi.core.annotation.Action;
import org.yi.web.base.BaseController;

import com.baidu.ueditor.ActionEnter;
import com.jfinal.kit.PathKit;

@Action(action = "/ueditor")
public class UploadController extends BaseController {
	
	public void upload() throws Exception {
		getRequest().setCharacterEncoding( "utf-8" );
		getResponse().setHeader("Content-Type" , "text/html");
		
		String rootPath = PathKit.getWebRootPath();
		
		renderJson(new ActionEnter( getRequest(), rootPath ).exec());
	}
}
