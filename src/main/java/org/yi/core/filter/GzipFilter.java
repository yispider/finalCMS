package org.yi.core.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.yi.core.common.Constants;
import org.yi.core.utils.StringUtils;

public class GzipFilter implements Filter {
    /**
     * 过滤器配置
     */
    private FilterConfig filterConfig = null;

    /**
     * 构造方法
     * 
     * @return 过滤器配置
     */
    protected final FilterConfig getFilterConfig() {
        return this.filterConfig;
    }

    /**
     * {@inheritDoc}
     * 
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    /**
     * {@inheritDoc}
     * 
     * @see javax.servlet.Filter#destroy()
     */
    @Override
    public void destroy() {
        this.filterConfig = null;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,
            ServletException {

        if (Constants.config.getBoolean("gzip",false)) {
            chain.doFilter(req, res);
        } else {
            if (req instanceof HttpServletRequest) {
            	// 排除不需要gzip的url
                if(!StringUtils.isInclude(((HttpServletRequest) req).getRequestURI(), 
                						Constants.config.getStringArray("gzip.exclude"))) {
                    HttpServletRequest request = (HttpServletRequest) req;
                    HttpServletResponse response = (HttpServletResponse) res;
                    String ae = request.getHeader("accept-encoding");
                    if (ae != null && ae.indexOf("gzip") != -1) {
                        GZIPResponseWrapper wrappedResponse = new GZIPResponseWrapper(response);
                        chain.doFilter(req, wrappedResponse);
                        wrappedResponse.finishResponse();
                        return;
                    }
                }
            }
            chain.doFilter(req, res);
        }
    }
}