package org.yi.core.helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.minidev.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yi.core.common.Constants;
import org.yi.core.utils.CookieUtils;
import org.yi.web.account.entity.AccountEntity;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.MACSigner;

public class DuoshuoHelper {
	
	private static final Logger logger = LoggerFactory.getLogger(DuoshuoHelper.class);
	
	public static void token(AccountEntity account, HttpServletRequest request, HttpServletResponse response) {
		JSONObject userInfo = new JSONObject();
        
        userInfo.put("short_name", Constants.config.getString("duoshuo.short_name"));//必须项
        userInfo.put("user_key", account.get("id"));//必须项
        userInfo.put("name", account.get("login_name"));//可选项
        
        Payload payload = new Payload(userInfo);
        
        JWSHeader header = new JWSHeader(JWSAlgorithm.HS256);
        
        // Create JWS object
        JWSObject jwsObject = new JWSObject(header, payload);
        try {
        	// Create HMAC signer
        	JWSSigner signer = new MACSigner(Constants.config.getString("duoshuo.secret").getBytes());
            jwsObject.sign(signer);
            // Serialise JWS object to compact format
            String token = jwsObject.serialize();
            CookieUtils.addCookie("duoshuo_token", token, request.getServerName(), Constants.config.getInt("cookie.period", 3600), response);
        } catch (JOSEException e) {
        	logger.error("产生duoshuo本地身份token出错", e);
            return;
        }        
        
	}

}
