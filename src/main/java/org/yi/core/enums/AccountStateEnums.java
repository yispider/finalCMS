package org.yi.core.enums;

public enum AccountStateEnums {

	NORMAL(1, "normal"), 
	LOCK(0, "lock");
	
	private Integer code;
	private String desc;
	
	private AccountStateEnums(Integer code, String desc) {
		this.code = code;
		this.desc = desc;
	}
	
	public static AccountStateEnums parseEnum(Integer c){
		for(AccountStateEnums p : values()){
			if(p.getCode() == c) {
				return p;
			}
		}
		return NORMAL;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
