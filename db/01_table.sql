/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.40 : Database - finalcms
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`finalcms` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `finalcms`;

/*Table structure for table `f_account` */

DROP TABLE IF EXISTS `f_account`;

CREATE TABLE `f_account` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `login_name` varchar(32) NOT NULL,
  `passwd` varchar(32) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `role` varchar(10) NOT NULL COMMENT 'admin, user',
  `phone` varchar(25) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `login_time` datetime DEFAULT NULL COMMENT 'last login time',
  `state` int(11) DEFAULT '1' COMMENT '1:normal\n0:lock',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_account` */

/*Table structure for table `f_category` */

DROP TABLE IF EXISTS `f_category`;

CREATE TABLE `f_category` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `level` varchar(45) DEFAULT NULL,
  `desc` varchar(120) DEFAULT NULL COMMENT 'desription',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_category` */

/*Table structure for table `f_friendlink` */

DROP TABLE IF EXISTS `f_friendlink`;

CREATE TABLE `f_friendlink` (
  `id` bigint(8) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `logo` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_friendlink` */

/*Table structure for table `f_param` */

DROP TABLE IF EXISTS `f_param`;

CREATE TABLE `f_param` (
  `id` bigint(8) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  `desc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `f_param` */

/*Table structure for table `f_permission` */

DROP TABLE IF EXISTS `f_permission`;

CREATE TABLE `f_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '权限名称',
  `description` varchar(255) DEFAULT NULL COMMENT '权限描述',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_roles_permission` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色表';

/*Data for the table `f_permission` */

/*Table structure for table `f_role` */

DROP TABLE IF EXISTS `f_role`;

CREATE TABLE `f_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) NOT NULL COMMENT '角色名',
  `role_desc` varchar(255) DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_roles` (`role_desc`,`role_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

/*Data for the table `f_role` */

/*Table structure for table `f_role_permission` */

DROP TABLE IF EXISTS `f_role_permission`;

CREATE TABLE `f_role_permission` (
  `id` bigint(11) NOT NULL,
  `role_id` bigint(11) NOT NULL COMMENT '角色id',
  `permission_id` bigint(11) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_permissin_u_key` (`role_id`,`permission_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限中间表';

/*Data for the table `f_role_permission` */

/*Table structure for table `f_user_role` */

DROP TABLE IF EXISTS `f_user_role`;

CREATE TABLE `f_user_role` (
  `id` bigint(11) NOT NULL,
  `user_id` bigint(20) NOT NULL COMMENT 'agent id',
  `role_id` bigint(11) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userId_roleId_unique_index` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色中间表';

/*Data for the table `f_user_role` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
